package com.bean;

import org.springframework.stereotype.Component;

@Component
public class UserDetails {
	private String Name;
	private String EmailId;
	private String Password;
	private String MobileNumber;
	private int UserId;

	public UserDetails() {
	}

	public UserDetails(String Name, String EmailId, String Password, String MobileNumber, int UserId) {
		super();
		this.Name = Name;
		this.EmailId = EmailId;
		this.Password = Password;
		this.MobileNumber = MobileNumber;
		this.UserId = UserId;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		this.EmailId = emailId;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	public int getUserId() {
		return UserId;
	}

	public void setUserId(int userId) {
		UserId = userId;
	}
	@Override
	public String toString() {
		return "UserDetails [Name=" + Name + ", EmailId=" + EmailId+ ", Password=" + Password + ", MobileNumber=" + MobileNumber
				+"]";
	}

}
