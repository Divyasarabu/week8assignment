package com.bean;

public class login {
	private String MobileNumber;
	private String Password;
	
	public login() {
		System.out.println("login");
	}
	public login(String MobileNumber, String Password) {
		super();
		this.MobileNumber = MobileNumber;
		this.Password = Password;
	}
	public String getMobileNumber() {
		return MobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	@Override
	public String toString() {
		return "login [MobileNumber=" + MobileNumber + ", Password=" + Password + "]";
	}
	
	

}
