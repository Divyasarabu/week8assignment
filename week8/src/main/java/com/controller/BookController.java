package com.controller;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.DB.*;
import com.bean.book;


@Controller

public class BookController {
	@Autowired
	private BookDB bookDB;
	

	public BookController() {
		// TODO Auto-generated constructor stub
	}

	@GetMapping("/books")
	public String mainbooks(HttpSession session) {
	
		try {
			List<book> books= bookDB.getAllBooks();
			session.setAttribute("books", books);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "books";
	}
	
	@GetMapping("/likedbooks")
	public String likedBooks(HttpSession session) {
		String email = (String) session.getAttribute("email");
		if(email!=null) {
		List<book> books = bookDB.getlikedBooks(email);
		session.setAttribute("books", books);
		session.setAttribute("from", "likedbooks");
		return "books2";
		}
		else
		return "login";
		
		
	}
	@GetMapping("/readLaterbooks")
	public String readLaterBooks(HttpSession session) {
		String MobileNumber = (String) session.getAttribute("MobileNumber");
		if(MobileNumber!=null) {
		List<book> books = bookDB.getReadLaterBooks(MobileNumber);
		session.setAttribute("books", books);
		session.setAttribute("from", "readLaterbooks");
		return "books2";
		}
		else
		return "login";
	}
	
	@GetMapping("/favbutton")
	public String getLiked() {
		return "books";
	}
	
	@PostMapping("/favbutton")
	public String liked(@ModelAttribute("bookId") int bookId,HttpSession session) {
		System.out.println(bookId);
		String MobileNumber = (String) session.getAttribute("MobileNumber");
		if(MobileNumber!=null) {
		bookDB.addFavBook(bookId, MobileNumber);
		return "books";
		}
		else
		return "login";
	}
	
	@PostMapping("/removefav")
	public String removefav(@ModelAttribute("bookId") int bookId,HttpSession session) {
		String MobileNumber = (String) session.getAttribute("MobileNumber");
		
		try {
			bookDB.removefavbook(bookId, MobileNumber);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<book> books = bookDB.getlikedBooks(MobileNumber);
		session.setAttribute("books", books);
		session.setAttribute("from", "likedbooks");
		return "books2";
	}
	
	@GetMapping("/readlaterbutton")
	public String getReadLater() {
		return "books";
	}
	
	@PostMapping("/readlaterbutton")
	public String readlater(@ModelAttribute("bookId") int bookId,HttpSession session) {
		
		System.out.println(bookId);
		String MobileNumber = (String) session.getAttribute("MobileNumber");
		if(MobileNumber!=null) {
		bookDB.addReadlaterBook(bookId, MobileNumber);
		return "books";
		}
		else
		return "login";
	}
	
	@PostMapping("/removereadlater")
	public String removereadlater(@ModelAttribute("bookId") int bookId,HttpSession session) {
		String MobileNumber = (String) session.getAttribute("MobileNumber");
		
		try {
			bookDB.removeReadlaterBook(bookId, MobileNumber);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<book> books = bookDB.getReadLaterBooks(MobileNumber);
		session.setAttribute("books", books);
		session.setAttribute("from", "readLaterbooks");
		return "books2";
	}
	
	@GetMapping("/removefav")
	public String getRemoveFav() {
		return "likedbooks";
	}
	@GetMapping("removereadlater")
	public String getRemoveReadlater() {
		return "readLaterbooks";
	}
	

}
