package com.DB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bean.book;
import com.DB.*;


@Repository
public class BookDB {
	@Autowired
	private JdbcTemplate template;
	@Autowired
	private UserDB udb;

	public List<book> getAllBooks() throws SQLException {
		
		String sql = "select  * from book";
		
		System.out.println(this.template);
		return  this.template.query(sql,new RowMapper<book>() {
			@Override
			public book mapRow(ResultSet rs, int arg1) throws SQLException {
					book book = new book();
					book.setBook_Id(rs.getInt(1));
					book.setTitle(rs.getString(2));
					book.setStoryline(rs.getString(3));
					book.setImage(rs.getString(4));
					
					return book;		
			}
		});
	}
	
	public List<book> getlikedBooks(String MobileNumber){
		int userId = 0;
		try {
			userId = udb.getUserId(MobileNumber);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String sql = "select distinct book.book_Id, title, Storyline ,Image from book,likedbooks where book.book_Id=likedbooks.book_Id"
				+ " and likedbooks.userId="+userId;
		return this.template.query(sql,new RowMapper<book>() {
			@Override
			public book mapRow(ResultSet rs, int arg1) throws SQLException {
				book book = new book();
				book.setBook_Id(rs.getInt(1));
				book.setTitle(rs.getString(2));
				book.setStoryline(rs.getString(3));
				book.setImage(rs.getString(4));
				
				return book;
			}
		});
	}
	
	public List<book> getReadLaterBooks(String MobileNumber){
		int userId = 0;
		try {
			userId = udb.getUserId(MobileNumber);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String sql = "select distinct book.bookId, title, Storyline, Image from book,readlaterbooks,user "
				+ "where book.bookId=readlaterbooks.bookId and readlaterbooks.userId="+userId;
		return this.template.query(sql,new RowMapper<book>() {
			@Override
			public book mapRow(ResultSet rs, int arg1) throws SQLException {
				book book = new book();
				book.setBook_Id(rs.getInt(1));
				book.setTitle(rs.getString(2));
				book.setStoryline(rs.getString(3));
				book.setImage(rs.getString(4));
				return book;
			}
		});
	}
	
	public boolean addFavBook(int bookId,String MobileNumber) {
		int userId = 0;
		try {
			userId = udb.getUserId(MobileNumber);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String sql = "insert into likedbooks(bookId,userId) value(?,?)";
		this.template.update(sql,bookId,userId);
		return true;
	}
	
	public boolean addReadlaterBook(int bookId,String MobileNumber) {
		int userId = 0;
		try {
			userId = udb.getUserId(MobileNumber);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String sql = "insert into readlaterbooks(bookId,userId) value(?,?)";
		this.template.update(sql,bookId,userId);
		return true;
	}
	
	public boolean removefavbook(int bookId,String MobileNumber) throws SQLException {
		String sql = "delete from likedbooks where bookId=? and userId=?";
		int userId = udb.getUserId(MobileNumber);
		this.template.update(sql,bookId,userId);
		return true;
	}
	
	public boolean removeReadlaterBook(int bookId,String MobileNumber) throws SQLException {
		String sql = "delete from readlaterbooks where bookId=? and userId=?";
		int userId = udb.getUserId(MobileNumber);
		this.template.update(sql,bookId,userId);
		return true;
	}
}
