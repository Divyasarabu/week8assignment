package com.DB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bean.*;
import com.DB.*;

@Repository
public class UserDB {
	@Autowired
	private JdbcTemplate template;

	public boolean insertUser(UserDetails user) throws SQLException {
		
		String sql = "insert into user(Name, EmailId,MobileNumber,Password) values(?,?,?,?)";

		template.update(sql, user.getName(), user.getEmailId(), user.getMobileNumber(), user.getPassword());

		return true;
	}


	public boolean validate(login loginUser) {

		String mobileNumber= loginUser.getMobileNumber(); 
		String pwd= loginUser.getPassword();
		String sql = "select EmailId, Password, MobileNumber, Name from user where MobileNumber = ?";
		UserDetails user;
		try{
			user = this.template.queryForObject(sql, new UserRowMapper(), mobileNumber);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		if(user.getMobileNumber().equals(mobileNumber) && user.getPassword().equals(pwd))
			return true;
		return false;
	}

	public int getUserId(String mobileNumber) throws SQLException {

		String sql = "select userId from user where MobileNumber ='" + mobileNumber+ "';";
		List<UserDetails> list = this.template.query(sql, new RowMapper<UserDetails>() {
			@Override
			public UserDetails mapRow(ResultSet rs, int arg1) throws SQLException {
				UserDetails user = new UserDetails();
				user.setUserId(rs.getInt(1));
				return user;	
}
		});
		int userId=0;
		for(UserDetails user:list) {
			userId = user.getUserId();
		}
		return userId;
	}
	class UserRowMapper implements RowMapper<UserDetails>{

	@Override
	public UserDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserDetails user = new UserDetails();
		user.setName(rs.getString(1));
		user.setEmailId(rs.getString(2));
		user.setPassword(rs.getString(3));
		user.setMobileNumber(rs.getString(4));
		return user;
	}
	}
}
