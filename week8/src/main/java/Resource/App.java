package Resource;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import Resource.WebAppConfig;

public class App  extends AbstractAnnotationConfigDispatcherServletInitializer {

	public App() {
		System.out.println("app initalizer loaded");
	}

    @Override
     protected Class<?>[] getRootConfigClasses() {
          return null;
     }

     @Override
     protected Class<?>[] getServletConfigClasses() {
    	 System.out.println("app initializer");
         return new Class<?>[] { WebAppConfig.class };
     }

     @Override  
     protected String[] getServletMappings() {
          return new String[] { "/" };
     }
}


