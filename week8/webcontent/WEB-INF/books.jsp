<%@page import="java.util.Arrays"%>
<%@page import="com.bean.*"%>
<%@page import="com.DB.*"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Books</title>
<link rel="stylesheet" href="assets/styles.css" type="text/css">
</head>
<body>
	<div class="nav" id="nav" style="background-color: transparent;">
		<a href="books">books</a>
		<div class="join-box">
			<p class="join-msg">
				<a href="readLaterbooks">Read Later</a> <a href="likedbooks">Liked</a>
				<%
				String mobileNumber = (String) session.getAttribute("MobileNumber");
				if (mobileNumber == null) {
				%>
				<a href="login">Sign in</a>
				<%
				} else {
				%>
				<a><%=mobileNumber%></a> <a href="logout">Log Out</a>
				<%
				}
				%>
			</p>
		</div>
	</div>

	<div class='content'>
		<%
		List<book> books = (List<book>) session.getAttribute("books");
		for (book book : books) {
		%>
		<div class='row'>
			
			<p>
				Author :<%=book.getStoryline()%></p>
			<form action="favbutton" method="post">
				<input type="hidden" id="thisField" name="book_Id"
					value="<%=book.getbook_Id()%>">

				<button type="submit" class="fav">
					Like</button>
			</form>
			<form action="readlaterbutton" method="post">
				<input type="hidden" id="thisField" name="book_Id"
					value="<%=book.getbook_Id()%>">

				<button type="submit" class="readLater">
					Read Later</button>
			</form>
			


		</div>
		<%
		}
		%>

	</div>
</body>
</html>